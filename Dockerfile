FROM ruby:2.6.3

RUN apt-get update
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
        && apt-get install -y nodejs

RUN mkdir -p /setup
WORKDIR /setup
COPY ./Gemfile /setup
RUN bundle install

RUN mkdir -p /app
RUN rm -rf /setup
WORKDIR /app
COPY . /app
CMD bundle exec rails s -p 3000 -b '0.0.0.0'