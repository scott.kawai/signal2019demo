require 'sendgrid-ruby'
include SendGrid

class HomeController < ApplicationController
    def index
        @name = params[:name]
        @email = params[:email]
        @error = params[:error]
    end

    def signup
        input = params.permit([:name, :email]).to_h

        # We'll want to send a welcome email here.

        redirect_to action: "welcome"
    end

    def welcome
    end
end
